<!DOCTYPE html>
<head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css"  >

    <!-- Custom styles for this template -->
    <link href="../css/cover.css" rel="stylesheet">
    
    <title>Home Page - Bootstrap</title>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SESSION -->
            <?php 
				session_start();

				if(!isset($_SESSION['name']) && !isset($_SESSION['pass'])){
					unset($_SESSION['name']);
					unset($_SESSION['pass']);
					header("Location: ../index.php");
				}

        $logou = $_SESSION['name'];
            ?>
   <!-- Fim -->

   

</head>

<body class="text-center">



    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h5 class="masthead-brand"><?php echo $_SESSION['name']; ?>
          <svg class="bd-placeholder-img mr-2 rounded" width="42" height="42" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32">
            <title><?php echo $_SESSION['name']; ?></title>
            <img src="https://img.icons8.com/clouds/32/000000/edit-user.png">
            <text x="50%" y="50%" fill="#007bff" dy=".3em"></text>
        </svg>
        </h3>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link " href="user.php">Home</a>
            <a class="nav-link" href="#">Features</a>
            <a class="nav-link active" href="cadastro.php">Cadastro</a>
            <a class="nav-link" href="../php/logout.php">Logout</a>
          </nav>
        </div>
      </header>

      <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-lg-4">
                <form class="form-signin" action="php/verif.php" method="POST">    
                <div class="col col-lg-4">
                <label for="firstName">First name</label>
                <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                </div>

                    <button class="button-form" type="submit">Login</button>
                </form>
            </div> <!-- login -->
        </div> <!-- center -->
    </div> <!-- Container -->

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Cover template for <a href="https://getbootstrap.com/">Bootstrap</a>, by <a href="https://twitter.com/mdo">@mdo</a>.</p>
        </div>
      </footer>
    </div>
</body>

<!-- Bootstrap JS -->
<script src="../js/jquery-3.4.1.js"></script>
<script src="../js/bootstrap.min.js"></script>

</html>