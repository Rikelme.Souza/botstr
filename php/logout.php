<?php

	session_start();

	unset($_SESSION['name']);
	unset($_SESSION['pass']);

	session_destroy();

	echo "
			<script>

				alert('Deslogado com sucesso');
				window.location = '../index.php';

			</script>
		";

?>