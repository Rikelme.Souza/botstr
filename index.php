<!DOCTYPE html>
<head>
<!-- Bootstrap CSS -->
<link href="css/signin.css" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css"  >


    <title>Login - Bootstrap</title>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
</head>

<body class="text-center"> 

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-lg-3">
                <form class="form-signin" action="php/verif.php" method="POST">
                    <h2 class="h3 mb-3 font-weight-normal" >Clothes Show</h2>
                    <label for="inputName" class="sr-only">Nome Usuário</label>
                    <input type="name" name="name" id="inputName" class="form-control" placeholder="Seu Nome Usuário" required autofocus>
                    <label for="inputPassword" class="sr-only">Sua Senha</label>
                    <input type="password" name="pass" id="inputPassword" class="form-control" placeholder="Sua Senha" required>
                    <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                    </div>
                    <button class="button-form" type="submit">Login</button>
                </form>
            </div> <!-- login -->
        </div> <!-- center -->
    </div> <!-- Container -->

</body>

<!-- Bootstrap JS -->
<script src="js/jquery-3.4.1.js"></script>
<script src="js/bootstrap.min.js"></script>


</html>